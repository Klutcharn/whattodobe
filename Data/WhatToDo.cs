﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;


namespace WebAPI.Data
{
    public class WhatToDo : DbContext
    {
        //Adds tables to the database
        public DbSet<User> Users { get; set; }
        public DbSet<Activity> Activities { get; set; }
      

        //Creates base options for the database
        public WhatToDo(DbContextOptions options) : base(options)
        {
        }

        //Seeding data on creation of the database
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    UserId = 1,
                    Username = "Martin",
                    Password = "123",
                    Completed = 0
                });
            modelBuilder.Entity<Activity>()
                .HasData(new Activity
                {
                    ActivityId=1,
                    Desc = "Learn Python",
                    Finished = false,
                    UserId = 1,
                });
          
            modelBuilder.Entity<Activity>()
                .HasOne(k => k.User)
                .WithMany(k => k.Activities)
                .HasForeignKey(k => k.UserId)
                .OnDelete(DeleteBehavior.Cascade);


        }


    }
}
