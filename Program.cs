using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Reflection;
using WebAPI.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "HvZAPI", Version = "v1" });

  
});
builder.Services.AddDbContext<WhatToDo>(options =>
   options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsApi", builder => builder.WithOrigins("http://localhost:3000", "http://localhost:3000/game", "http://localhost:3000/admin", "https://hvzgame.z16.web.core.windows.net", "https://hvzgame.z16.web.core.windows.net/", "https://hvzgame.z16.web.core.windows.net/*", "https://hvzgame.z16.web.core.windows.net/game", "https://hvzgame.z16.web.core.windows.net/admin")
     .AllowAnyHeader()
     .AllowAnyMethod());
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApplication2 v1"));
}

app.UseHttpsRedirection();

app.UseCors("CorsApi");

app.UseAuthorization();

app.MapControllers();

app.Run();