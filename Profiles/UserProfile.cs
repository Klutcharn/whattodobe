﻿using AutoMapper;
using WebAPI.Models;
using What_to_do.Models.DTO.User;

namespace What_to_do.Profiles
{
    public class UserProfile: Profile
    {

        public UserProfile()
        {
            //Creates maps between two different classes to only show relevant and objective data about each model, one for each DTO
            CreateMap<User, CreateUserDTO>()
                .ReverseMap();
            CreateMap<User, EditUserDTO>()
                .ReverseMap();
            CreateMap<User, ReadUserDTO>()
              .ReverseMap();
        }

    }
}
