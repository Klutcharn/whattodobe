﻿using AutoMapper;
using WebAPI.Models;
using What_to_do.Models.DTO.NewFolder;

namespace What_to_do.Profiles
{
    public class ActivityProfile: Profile
    {
        public ActivityProfile()
        {
            //Creates maps between two different classes to only show relevant and objective data about each model, one for each DTO
            CreateMap<Activity, ReadActivityDTO>()
                .ReverseMap();
            CreateMap<Activity, EditActivityDTO>()
               .ReverseMap();
            CreateMap<Activity, CreateActivityDTO>()
               .ReverseMap();
        }
        
    }
}
