﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;
using What_to_do.Models.DTO.NewFolder;
using What_to_do.Models.DTO.User;

namespace What_to_do.Controllers
{
    [EnableCors("CorsApi")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly WhatToDo _context;
        private readonly IMapper _mapper;

        public UsersController(WhatToDo context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Users
        [HttpGet]
        [Route("user")]
        public async Task<ActionResult<List<ReadUserDTO>>> GetUsers()
        {
            var user = _context.Users.Include(g => g.Activities);
            List<User> users = new List<User>();

            foreach(User User in user)
            {
                users.Add(User);
            }
            return _mapper.Map<List<ReadUserDTO>>(user);
       
        }

        // GET: api/Users/5
        [HttpGet]
        [Route("user/{userid}")]
        public async Task<ActionResult<ReadUserDTO>> GetUser(int userid)
        {
            var user = _context.Users.Include(u => u.Activities).FirstOrDefault(p => p.UserId == userid);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadUserDTO>(user);
        }

        // GET: api/Users/5
        [HttpGet]
        [Route("user/{userid}/activities")]
        public async Task<ActionResult<List<ReadActivityDTO>>> GetAllActivities(int userid)
        {

            var user = _context.Users.Include(g => g.Activities).FirstOrDefault(p => p.UserId == userid);
            List<Activity> chosen_activities = new List<Activity>();

            if (user == null)
            {
                return NotFound();
            }
            if (user.Activities == null)
            {
                return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
            }
           

            foreach (Activity activity in user.Activities)
            {

                chosen_activities.Add(activity);

            }

            return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
        }


        [HttpPut]
        [Route("user/{userid}/activity/{activityid}")]
        public async Task<IActionResult> PutActivity(int userid, int activityid, EditActivityDTO dtoActivity)
        {

            Activity domainActivity = _mapper.Map<Activity>(dtoActivity);
            domainActivity.UserId = userid;
            domainActivity.ActivityId = activityid;
            _context.Entry(domainActivity).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(userid))
                {
                    return NotFound();
                }
                else if (!ActivityExists(activityid))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }


   


        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [Route("user/{userid}")]
        public async Task<IActionResult> PutUser(int userid, User user)
        {
            if (userid != user.UserId)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(userid))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("user")]
        public async Task<ActionResult<User>> PostUser(CreateUserDTO dtoUser)
        {
            User userDomain = _mapper.Map<User>(dtoUser);
            _context.Users.Add(userDomain);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(UsersController.GetUser), new { userid = userDomain.UserId },_mapper.Map<CreateUserDTO>(userDomain));
        }

        [HttpPost]
        [Route("user/{userid}/activity")]
        public async Task<ActionResult<Activity>> PostSquad(CreateActivityDTO dtoActivity, int userid)
        {

            Activity activityDomain = _mapper.Map<Activity>(dtoActivity);
            activityDomain.UserId = userid;
            _context.Activities.Add(activityDomain);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Users/5
        [HttpDelete]
        [Route("user/{userid}")]
        public async Task<IActionResult> DeleteUser(int userid)
        {
            var user = await _context.Users.FindAsync(userid);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }
        private bool ActivityExists(int id)
        {
            return _context.Activities.Any(e => e.ActivityId == id);
        }

        [HttpDelete]
        [Route("user/{userid}/activity/{activityid}")]
        public async Task<IActionResult> DeleteActivity(int userid, int activityid)
        {
            var user = await _context.Users.FindAsync(userid);
            var activity = await _context.Activities.FindAsync(activityid);
            if (user == null)
            {
                return NotFound();
            }
            if (activity == null)
            {
                return NotFound();
            }
            _context.Activities.Remove(activity);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        //// GET: api/Users/5
        //[HttpGet]
        //[Route("user/{userid}/allunfinishedactivities")]
        //public async Task<ActionResult<List<ReadActivityDTO>>> GetAllUnfinishedActivities(int userid)
        //{

        //    var user = _context.Users.Include(g => g.Activities).FirstOrDefault(p => p.UserId == userid);
        //    List<Activity> chosen_activities = new List<Activity>();

        //    if (user == null)
        //    {
        //        return NotFound();
        //    }
        //    if (user.Activities == null)
        //    {
        //        return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
        //    }


        //    foreach (Activity activity in user.Activities)
        //    {
        //        if (activity.Finished == false)
        //        {
        //            chosen_activities.Add(activity);
        //        }
        //    }

        //    return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
        //}

        //// GET: api/Users/5
        //[HttpGet]
        //[Route("user/{userid}/allfinishedactivities")]
        //public async Task<ActionResult<List<ReadActivityDTO>>> GetAllFinishedActivities(int userid)
        //{

        //    var user = _context.Users.Include(g => g.Activities).FirstOrDefault(p => p.UserId == userid);
        //    List<Activity> chosen_activities = new List<Activity>();

        //    if (user == null)
        //    {
        //        return NotFound();
        //    }
        //    if (user.Activities == null)
        //    {
        //        return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
        //    }


        //    foreach (Activity activity in user.Activities)
        //    {
        //        if(activity.Finished == true) { 
        //        chosen_activities.Add(activity);
        //        }
        //    }

        //    return _mapper.Map<List<ReadActivityDTO>>(chosen_activities);
        //}



        //Post activity


    }
}
