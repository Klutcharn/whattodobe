﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Activity
    {
        public int ActivityId { get; set; }
        public string Desc { get; set; }
        public bool Finished { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
