﻿using WebAPI.Models;
using What_to_do.Models.DTO.NewFolder;

namespace What_to_do.Models.DTO.User
{
    public class ReadUserDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string? Email { get; set; }
        public int Completed { get; set; }
        public ICollection<ReadActivityDTO> Activities { get; set; }
    }
}
