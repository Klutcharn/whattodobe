﻿namespace What_to_do.Models.DTO.NewFolder
{
    public class EditActivityDTO
    {
        public string Desc { get; set; }
        public bool Finished { get; set; }
    }
}
