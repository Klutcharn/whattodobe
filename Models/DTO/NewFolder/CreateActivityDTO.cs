﻿namespace What_to_do.Models.DTO.NewFolder
{
    public class CreateActivityDTO
    {
        public string Desc { get; set; }
        public bool Finished { get; set; }  
    }
}
