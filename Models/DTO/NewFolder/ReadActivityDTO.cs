﻿namespace What_to_do.Models.DTO.NewFolder
{
    public class ReadActivityDTO
    {
        public int ActivityId { get; set; }
        public string Desc { get; set; }
        public bool Finished { get; set; }
    }
}
